# README - CRUD api

## Get started

### Clone
First off, grab the repo by running `git clone git@bitbucket.org:cfoxeu/rubber_ducks.git`

### Check for yarn
To check if you have yarn installed type `yarn --version`

Check out how to get it [here](https://yarnpkg.com/en/docs/install)

### Install
Navigate into the project directory and run `yarn` to install dependencies

### Database access
Currently this can be done by creating the following file:
```
// ./app/config/database.config.js

module.exports = {
    url: 'mongodb://<USER>:<PASSWORD>@ds113019.mlab.com:13019/crud-api-test'
}
```
`<USER>` and `<PASSWORD>` will need to be replaced with a valid login combination defined by the Mongo db

You could also hook this up to your own mongodb, you can find out more about this [here](https://www.mlab.com)

### Running the server
Now you can run the server locally with `yarn start`

### Testing the server
If there is trouble afoot, try running `yarn test` to test the application

## Get stuck in

### Where to see the good stuff
You can access the api straight away by visiting localhost:3000 in your browser

However I recommend using postman to test with, which you can find [here](https://www.getpostman.com/)

### Provided functionality
Now that your server is up and running, you can start working with the database

Currently exposed functionality:

All routes for the api must be prefixed with `/api`

Return a list of all rubber duck | { path: `/ducks` }

Return a single rubber duck object | { path: `/duck/duckID` }

Create a new duck | { path: `/ducks/create` }, { params: `?name=NAME&price=PRICE` }

Update an existing duck | { path: `/ducks/duckID/update` }, { params: `?name=NAME&price=PRICE` }

Delete an existing duck | { path: `/ducks/duckID/delete` }
