const Duck = require('../models/rubber_duck.model')

// By default all rubber_ducks will be found and returned
exports.findAll = (req, res) => {
    // Retrieve and return all ducks from the database.
    Duck.find((err, ducks) => {
        if (err) return res.status(500).send({ message: 'Some error occurred while retrieving ducks' })
        return res.send(ducks)
    })
}

// Create a new rubber_duck
exports.create = (req, res) => {
    // Check fields are not empty
    if (!req.query.name || !req.query.price) {
        return res.status(400).send({ message: 'All fields are required' })
    }
    // Check to see if name exists already
    Duck.findOne({ name: req.query.name }, (err, existingDuck) => {
        if (err) return res.status(500).send({ message: 'Error while cross-checking name' })
        if (existingDuck) return res.status(400).send({ message: 'A rubber duck with this name already exists in our records' })
        // Otherwise create new Duck object
        const duck = new Duck({
            name: req.query.name,
            price: req.query.price,
        })
        // Save duck to database
        duck.save((err, data) => {
            if (err) return res.status(500).send({ message: 'Error while creating a duck' })
            return res.send(data)
        })
    })
}

exports.findOne = (req, res) => {
    // Find a single duck with a duckId
    Duck.findById(req.params.duckId, (err, duck) => {
        if (err) {
            if (err.kind === 'ObjectId') return res.status(404).send({ message: "No duck found with id " + req.params.duckId })
            return res.status(500).send({ message: `Error retrieving duck with id ${req.params.duckId} Details: ${err}` })
        }
        if (!Duck) return res.status(404).send({ message: 'No duck found with id ' + req.params.duckId })
        res.send(duck)
    })
}

exports.update = (req, res) => {
    const { duckId } = req.params
    const { name, price } = req.query
    // Update a duck identified by the duckId in the request
    Duck.findById(duckId, (err, duck) => {
        if (err) {
            if (err.kind === 'ObjectId') return res.status(404).send({ message: "No duck found with id " + req.params.duckId })
            return res.status(500).send({ message: 'Error finding duck with id ' + duckId })
        }
        if (!Duck) return res.status(404).send({ message: 'No duck found with id ' + duckId })

        if (name) duck.name = name
        if (price) duck.price = price

        duck.save((err, data) => {
            if (err) return res.status(500).send({ message: 'Could not update Duck with id ' + duckId })
            res.send(data)
        })
    })
}

exports.delete = (req, res) => {
    const { duckId } = req.params
    // Delete a duck with the specified duckId in the request
    Duck.findByIdAndRemove(duckId, (err, duck) => {
        if (err) {
            if (err.kind === 'ObjectId') return res.status(404).send({ message: "No duck found with id " + req.params.duckId })
            return res.status(500).send({ message: 'Could not delete Duck with id ' + duckId })
        }
        if (!Duck) return res.status(404).send({ message: 'Duck not found with id ' + duckId })
        res.send({ message: 'Duck deleted successfully!' })
    })
}
