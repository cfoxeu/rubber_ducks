const express = require('express')
const bodyParser = require('body-parser')
const mongoose = require('mongoose')

// Create express server
const server = express()

// Parse requests of content-type - application/x-www-form-urlencoded
server.use(bodyParser.urlencoded({ extended: true }))

// Parse requests of content-type - application/json
server.use(bodyParser.json())

// Configuring the database
const dbConfig = require('./config/database.config.js')
mongoose.Promise = global.Promise
mongoose.connect(dbConfig.url)

// Log if unable to connect and exit
mongoose.connection.on('error', () => {
    console.log('Could not connect to the database. Exiting now...')
    process.exit()
})

// Log once connection is established
mongoose.connection.once('open', () => {
    console.log('Successfully connected to the database')
})

// Expose ducks controller logic
const ducks = require('./app/controllers/rubber_duck.controller')

// Create simple welcome message
const welcomeMessage = { message: 'Welcome to the Rubber Ducks express API, please use /ducks to access data' }

// Get an instance of the express Router
var router = express.Router()

// middleware to use for all requests
router.use((req, res, next) => {
    console.log('Something is happening...')
    // make sure we go to the next routes and don't stop here
    next()
})

// Define server ROUTES
router.get('/', (req, res) => { res.send(welcomeMessage) })
router.get('/ducks', ducks.findAll)
router.post('/ducks/create', ducks.create)
router.get('/ducks/:duckId', ducks.findOne)
router.put('/ducks/:duckId/update', ducks.update)
router.delete('/ducks/:duckId/delete', ducks.delete)

// Register ROUTES & prefix routes with /api
server.use('/api', router)

// Listen for requests on port 3000
server.listen(3000, () => { console.log('Server listening on port 3000!') })

// Finally export the server
module.exports = server
