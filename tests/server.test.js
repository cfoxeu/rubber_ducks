const request = require('supertest')
const server = require('../server')
const sinon = require('sinon')
const mongoose = require('mongoose')

const Duck = require('../app/models/rubber_duck.model.js')
const duckParams = '?name=Bill&price=123'
const duckParamsBlank = '?name=&price='

describe('Server routes', () => {
    let testId
    describe('GET API /api', () => {
        test('responds with status code 200', () => {
            return request(server).get('/api')
            .then(res => { expect(200) })
        })
    })
    describe('GET ALL DUCKS /api/ducks', () => {
        test('responds with status code 200', () => {
            return request(server).get('/api/ducks')
            .then(res => { expect(200) })
        })
    })

    //===//===//===//
    // Need a way to mock database tests to keep separate from live data
    // For now just pass testId from CREATE to other functions
    // This way only the test duck is affected
    //===//===//===//

    describe('CREATE DUCK /api/ducks/create', () => {
        describe('with invalid credentials', () => {
            test('responds with status code 400', () => {
                return request(server).post(`/api/ducks/create${duckParamsBlank}`)
                .then(res => { expect(400) })
            })
        })
        describe('with valid credentials', () => {
            test('responds with status code 200 and correct duck object', () => {
                const expectedDuck = {
                    name: 'Bill',
                    price: 123
                }
                return request(server).post(`/api/ducks/create${duckParams}`)
                .then(res => {
                    expect(res.statusCode).toBe(200)
                    expect(res.body.name).toBe(expectedDuck.name)
                    expect(res.body.price).toBe(expectedDuck.price)
                    // Set testId to _id for safe testing
                    testId = res.body._id
                })
            })
        })
    })

    describe('READ DUCK /api/ducks/:duckId', () => {
        describe('with invalid id', () => {
            test('responds with status code 404', () => {
                return request(server).get('/api/ducks/0')
                .then(res => {
                    expect(404)
                })
            })
        })
        describe('with valid id', () => {
            test('responds with status code 200', () => {
                return request(server).get(`/api/ducks/${testId}`)
                .then(res => { expect(200) })
            })
        })
    })

    describe('UPDATE DUCK /api/ducks/:duckId/update', () => {
        describe('with invalid id', () => {
            test('responds with status code 404', () => {
                return request(server).put('/api/ducks/0/update')
                .then(res => { expect(404) })
            })
        })
        describe('with valid id but invalid params', () => {
            test('responds with status code 500', () => {
                return request(server).put(`/api/ducks/${testId}/update${duckParamsBlank}`)
                .then(res => { expect(500) })
            })
        })
        describe('with valid id and valid params', () => {
            test('responds with status code 200', () => {
                return request(server).put(`/api/ducks/${testId}/update${duckParams}`)
                .then(res => { expect(200) })
            })
        })
    })

    describe('DELETE DUCK /api/ducks/:duckId/delete', () => {
        describe('with invalid id', () => {
            test('responds with status code 404', () => {
                return request(server).delete('/api/ducks/0/delete')
                .then(res => { expect(404) })
            })
        })
        describe('with valid credentials', () => {
            test('responds with status code 200', () => {
                return request(server).delete(`/api/ducks/${testId}/delete`)
                .then(res => { expect(200) })
            })
        })
    })
})
